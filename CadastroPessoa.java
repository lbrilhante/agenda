import java.io.*;

public class CadastroPessoa{

  public static void main(String[] args) throws IOException{
    //burocracia para leitura de teclado
    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;


    
    //instanciando objetos do sistema
    ControlePessoa umControle = new ControlePessoa();
    Pessoa umaPessoa = new Pessoa();
    char Resposta = 'S'; 
    String opcao;
    while(Resposta != 'N'){
    System.out.print("Para cadastrar uma pessoa digite 1\nPara remover digite 2\nPara pesquisar uma pessoa digite 3\n");
    entradaTeclado = leitorEntrada.readLine();
    opcao = entradaTeclado;
    switch(opcao){
	case "1":    
	
    		//interagindo com usuário
    		System.out.println("Digite o nome da Pessoa:");
    		entradaTeclado = leitorEntrada.readLine();
    		String umNome = entradaTeclado;
    		umaPessoa.setNome(umNome);
	
    		System.out.println("Digite o telefone da Pessoa:");
    		entradaTeclado = leitorEntrada.readLine();
    		String umTelefone = entradaTeclado;
    		umaPessoa.setTelefone(umTelefone);
    	
    		System.out.println("Digite a idade da Pessoa:");
    		entradaTeclado = leitorEntrada.readLine();
    		String umaIdade = entradaTeclado;
    		umaPessoa.setIdade(Integer.parseInt(umaIdade));
		
    		System.out.println("Digite o sexo da Pessoa:(M)(F)");
    		entradaTeclado = leitorEntrada.readLine();
    		String umSexo = entradaTeclado;
    		umaPessoa.setSexo(umSexo.charAt(0));
 
    		System.out.println("Digite o email da Pessoa:");
    		entradaTeclado = leitorEntrada.readLine();
    		String umEmail = entradaTeclado;
    		umaPessoa.setEmail(umEmail);
		
    		System.out.println("Digite o endereço da Pessoa:");
    		entradaTeclado = leitorEntrada.readLine();
    		String umEndereco = entradaTeclado;
    		umaPessoa.setEndereco(umEndereco);
	
    		System.out.println("Digite o rg da Pessoa:");
    		entradaTeclado = leitorEntrada.readLine();
    		String umRg = entradaTeclado;
    		umaPessoa.setRg(umRg);
		
    		System.out.println("Digite o cpf da Pessoa:");
    		entradaTeclado = leitorEntrada.readLine();
    		String umCpf = entradaTeclado;
    		umaPessoa.setCpf(umCpf);   
    		//adicionando uma pessoa na lista de pessoas do sistema
    		umControle.adicionar(umaPessoa);
			
    		//conferindo saída
    		System.out.println("=================================");
    		System.out.println("Pessoa adicionada com sucesso");
    		System.out.println("=)");
    		//...
		break;
	   case "2":
		System.out.println("Quem voce deseja remover? ");
		entradaTeclado = leitorEntrada.readLine();
		umNome = entradaTeclado;
		umControle.remover(umNome);
		break;
	   case "3":
		System.out.println("Quem voce deseja pesquisar? ");
		entradaTeclado = leitorEntrada.readLine();
		String outroNome = entradaTeclado;
		umControle.Pesquisar(outroNome);
		
 		break;  
	}//fim case
     System.out.println("Deseja retornar ao menu ? (S)(N)");
     entradaTeclado = leitorEntrada.readLine();
     Resposta = entradaTeclado.charAt(0);
    }//fim while
  }

}
